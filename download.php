<?php
require_once(dirname(__FILE__).'/lib/util.php');
date_default_timezone_set('Asia/Tokyo');
$debug = 0;

$shousai_data = array(
                      1 => "Toy(not battery)",
                      2 => "Cosmetics",
                      3 => "Candy",
                      4 => "Stationery",
);

try {
  $pdo = new PDO('mysql:host=localhost;dbname=amazon_shipping_tool;charset=utf8',
                 'amazon_user','amazon_user_pass',
                 array(PDO::ATTR_EMULATE_PREPARES => false));
} catch (PDOException $e) {
  exit('データベース接続失敗。'.$e->getMessage());
}
$pdo->beginTransaction();

try{
  $rate = $_POST['dollar_rate'];
  if($rate && is_numeric($rate) ){
    $sql = 'update dollar_rate set rate =:rate where id = 1';
    $stmt = $pdo -> prepare($sql);
    $stmt->bindValue(':rate', $rate, PDO::PARAM_INT);
    $stmt->execute();
  }

  if( $debug){
    print "<pre>";
    print_r($_POST);
    print "</pre>";
  }

  $head = "No,発送方法,発送情報通し番号,発送予定日,発送先会社名,発送先名,発送先住所,発送先CITY,発送先STATE,発送先郵便番号,発送先電話番号,発送先FAX番号,発送先国,発送先備考,発送元備考,発送元会社名,発送元名,発送元住所1,発送元住所2,発送元住所3,発送元市区町村,発送元都道府県,発送元郵便番号,発送元電話番号,発送元FAX番号,発送元補助1,発送元補助2,ご依頼主の指示事項,返送/転送方法コード,小包保管期間,小包転送先住所,小包転送先CITY,小包転送先STATE,小包転送先国,小包転送先郵便番号,小包転送先TEL,小包転送先FAX,内容品の種別,内容品総額,郵便物番号,郵便物総数,通関書類備考,内容品総重量,郵便料金,梱包材の種別,保険金額,詳細名1,数量1,重量1,単価1,詳細名2,数量2,重量2,単価2,詳細名3,数量3,重量3,単価3,詳細名4,数量4,重量4,単価4,詳細名5,数量5,重量5,単価5,詳細名6,数量6,重量6,単価6,詳細名7,数量7,重量7,単価7,詳細名8,数量8,重量8,単価8,詳細名9,数量9,重量9,単価9,詳細名10,数量10,重量10,単価10\n";
  $head = preg_split("/,/", $head);
  $body_array = array();
  $body_array[] = $head;
  $weight_data = get_product_sku_hash($pdo);
  #print_r($weight_data);
  $sorted_ids = array();
  foreach( $_POST['id'] as $id_str){
    $ids = preg_split("/-/", $id_str);
    $id=$ids[1];
    $sort = $_POST['sort-'. $id];
    $sorted_ids[$sort] = $id;
  }
  ksort($sorted_ids); 

  $seq = 0;
  foreach( $sorted_ids as $id ){
    $seq++;
    #$ids = preg_split("/-/", $id_str);
    #$id=$ids[1];
    #$order = find_order_by_id($pdo,$id);
    # 配送情報に主に使用
    $order = find_order_by_order_id_num($pdo,$id);
    # 商品情報,金額関連情報の取得
    $info = get_products_by_order_id_num($pdo,$id);

    $shipping_method = $_POST['shipping_method-' . $id];
    $sort            = $_POST['sort-'. $id];
    $weight          = $_POST['weight-'. $id];
    $country         = $_POST['country-'. $id];
    $shousai         = $_POST['shousai-'. $id];
    $price           = calc_shipping_price($country,$shipping_method,$weight);
    
    if($info['qty'] == 1 && $info['item_cnt'] == 1 && is_numeric($weight)){
      $sku = $info['items'][0]['sku'];
      #既存の重さ情報と異なる、もしくは未設定の場合に更新する
      if( @$weight_data[$sku]['weight'] != $weight){
        if($shipping_method == "331"){ #SP SALの場合のみ重さを保存する
          $stmt = $pdo->prepare("UPDATE  products SET weight = ? WHERE sku = ?;");
          $stmt->execute( array($weight, $sku) );
        }
        $shipping_methods = array($shipping_method);
        if($shipping_method == "111"){
          $shipping_methods[] = "231";
        }
        if($shipping_method == "231"){
          $shipping_methods[] = "111";
        }
       
        foreach($shipping_methods as $hassou){
          $stmt2 = $pdo->prepare("INSERT INTO products_weight (sku,shipping_method,weight)
                               VALUES (?,?,?)
                               ON DUPLICATE KEY UPDATE weight = ?;");
          $stmt2->execute( array($sku, $hassou,$weight,$weight) );
        }
        
       # INSERT products_weight (sku,shipping_method,weight) VALUES()
      }
      if( @$weight_data[$sku]['shousai_type'] != $shousai){
        $stmt = $pdo->prepare("UPDATE  products SET shousai_type = ? WHERE sku = ?;");
        $stmt->execute( array($shousai, $sku) );
      }
    }

    if($debug){
      print "--- id $id --- <BR/>";
      print "shipping_method $shipping_method <BR/>";
      print "sort : $sort <BR/>";
      print "weight: $weight <Br/>";
    }

    $tmp_array= array(
                      $seq,#"[order_item_id]?",#No
                      $shipping_method,#発送方法
                      $order['order_id'],#$id,#発送情報通し番号
                      date("Ymd"),#発送予定日
                      "",#発送先会社名
                      $order["recipient_name"],#$order['buyer_name'],#"[buymer_name]",#発送先名
                      $order['ship_address_1']." ".$order['ship_address_2']." ".$order['ship_address_3'],#"[ship_address_1]",#発送先住所
                      $order['ship_city'],#"[ship_city]",#発送先CITY
                      $order['ship_state'],#"[ship_address_2]",#発送先STATE
                      $order['ship_postal_code'],#"[ship_postal_code]",#発送先郵便番号
                      $order['buyer_phone_number'],#発送先電話番号
                      "",#発送先FAX番号
                      $order['ship_country'],#"[ship_country]",#発送先国
                      "",#発送先備考
                      "",#発送元備考
                      "GROWINGONE LTD.",#発送元会社名
                      "TOY SHOP JAPAN HOBBYONE",#発送元名
                      "1F",#発送元住所1
                      "Katakura town 1-44",#発送元住所2
                      "",#発送元住所3
                      "Seki city",#発送元市区町村
                      "Gifu",#発送元都道府県
                      "501-3963",#発送元郵便番号
                      "+81-575-46-9486",#発送元電話番号
                      "+81-575-46-9487",#発送元FAX番号
                      "",#発送元補助1
                      "",#発送元補助2
                      "1",#ご依頼主の指示事項
                      "0",#返送/転送方法コード
                      "30",#小包保管期間
                      "",#小包転送先住所
                      "",#小包転送先CITY
                      "",#小包転送先STATE
                      "",#小包転送先国
                      "",#小包転送先郵便番号
                      "",#小包転送先TEL
                      "",#小包転送先FAX
                      "1",#内容品の種別
                      floor($rate * $info['sum']),#内容品総額
                      1,#郵便物番号
                      1,#$info['qty'],#郵便物総数
                      "",#通関書類備考
                      $weight ,#内容品総重量
                      $price ,#郵便料金
                      "06",#梱包材の種別
                      0,#$shipping_method == "231" ? "" : floor($rate * $info['sum']),#保険金額
                      @$shousai_data[ $shousai ] ,#詳細名
                      $info['qty'],#数量1
                      ($weight/$info['qty'])-5,#重量1(段ボール分重量除く)
                      floor($rate * $info['sum']/$info['qty']),#単価1
                      );
    $body_array[] = $tmp_array;
  }
  $pdo->commit();  
}catch (Exception $e){
  $pdo->rollback();
  throw $e;
  exit(); 
}

if($debug ){
  foreach ($body_array as $fields) {
    foreach ($fields as $f) {
      print "$f ,";
    }
    print "<BR/>";
  }

}else{
  /*
  $fileName = "prefs.csv";
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename=' . $fileName);
  header('Content-Transfer-Encoding: binary');
  header('Content-Length: ' . filesize($fileName));
  echo mb_convert_encoding($body,"SJIS", "UTF-8");
  */

  ob_start();
  header('Content-Type: text/plain;charset=UTF-8');
  $stream = fopen('php://output', 'w');
  foreach ($body_array as $fields) {
    $fields = $fields;
    fputcsv($stream, $fields);
  }
  header("Content-Type: application/octet-stream");
  header("Content-Disposition: attachment; filename=output_".  date("mj_Hi").".csv");
  echo mb_convert_encoding(ob_get_clean(), 'SJIS', 'UTF-8');
}