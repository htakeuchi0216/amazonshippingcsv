<?php
require_once dirname(__FILE__).'/vendor/autoload.php';
require_once(dirname(__FILE__).'/spyc.php');
require_once(dirname(__FILE__).'/lib/util.php');
##
$user = 'amazon';
$pass = 'shipping'; 
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Private Page"');
    header('HTTP/1.0 401 Unauthorized');
    die("ログインするためには正しい入力情報が必要です");
} else {
  if ($_SERVER['PHP_AUTH_USER'] != $user || $_SERVER['PHP_AUTH_PW'] != $pass) {
   header('WWW-Authenticate: Basic realm="Private Page"');
   header('HTTP/1.0 401 Unauthorized');
   die("入力情報が一致しません");
  }
}
##

$data =array();
$pdo = create_db();

$products_info = get_product_sku_hash($pdo);
$config = Spyc::YAMLLoad("conf/conf.yaml");
$shipping = Spyc::YAMLLoad("conf/shipping.yml");
$orders = get_orders2($pdo);

## render処理
$loader = new Twig_Loader_Filesystem(__DIR__ . '/tmpl');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate("index.html.twig");
$template->display(
                   array(
                         ## order情報の取得
                         'orders' => $orders,
                         ## ドルレートの取得
                         'dollar_rate' => get_dollar_rate($pdo),
                         'config'      => $config,
                         'shipping'    => $shipping,
                         'products_info'=> $products_info,
                         'upload_success' => $_GET['sc']
                         )
);
