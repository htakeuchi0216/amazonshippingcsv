 CREATE TABLE dollar_rate (
      id int(10) NOT NULL AUTO_INCREMENT,
      rate int(10) NOT NULL,
      PRIMARY KEY (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE orders(
     id int(10) NOT NULL AUTO_INCREMENT,
     seq int(10) NOT NULL,
order_id      VARCHAR(255),
order_id_num VARCHAR(255),
order_item_id VARCHAR(255),
purchase_date  DATETIME,
payments_date  DATETIME,
buyer_email   VARCHAR(255),
buyer_name    VARCHAR(255),
buyer_phone_number  VARCHAR(255),
sku  VARCHAR(255),
product_name  VARCHAR(255),
quantity_purchased integer,
currency  VARCHAR(255),
item_price DECIMAL(8,3),
item_tax   DECIMAL(8,3),
shipping_price  DECIMAL(8,3),
shipping_tax DECIMAL(8,3),
gift_wrap_price DECIMAL(8,3),
gift_wrap_tax DECIMAL(8,3),
ship_service_level VARCHAR(255),
recipient_name VARCHAR(255),
ship_address_1 VARCHAR(255),
ship_address_2 VARCHAR(255),
ship_address_3 VARCHAR(255),
ship_city      VARCHAR(255),
ship_state    VARCHAR(255),
ship_postal_code VARCHAR(255),
ship_country    VARCHAR(255),
ship_phone_number VARCHAR(255),
gift_wrap_type    VARCHAR(255),
gift_message_text VARCHAR(255),
item_promotion_discount  DECIMAL(8,3),
item_promotion_id VARCHAR(255), 
ship_promotion_discount DECIMAL(8,3),
ship_promotion_id VARCHAR(255),
delivery_start_date  VARCHAR(255),
delivery_end_date  VARCHAR(255),
delivery_time_zone  VARCHAR(255),
delivery_Instructions  VARCHAR(255),
sales_channel  VARCHAR(255),
earliest_ship_date DATETIME,
latest_ship_date   DATETIME,
earliest_delivery_date DATETIME,
latest_delivery_date   DATETIME,
created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at  TIMESTAMP ,
 PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ALTER TABLE orders ADD COLUMN order_id_num VARCHAR(255);

CREATE TABLE products (
     id int(20) NOT NULL AUTO_INCREMENT,
     asin      VARCHAR(255),
     sku       VARCHAR(255) UNIQUE,
     name      VARCHAR(255),
     shousai_type int(10),
     weight    int(10),
     shipping_method int(10),
     created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     updated_at  TIMESTAMP ,
     PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ALTER TABLE products ADD COLUMN name VARCHAR(255);
-- ALTER TABLE products ADD COLUMN shousai_type int(10);

CREATE TABLE products_weight (
     id int(10) NOT NULL AUTO_INCREMENT,
     sku       VARCHAR(255) ,
     shipping_method int(10),
     weight    int(10),
     PRIMARY KEY (id),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE products_weight  ADD UNIQUE product_weight_unique_key(sku,shipping_method);