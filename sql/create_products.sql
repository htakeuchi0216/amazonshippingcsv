CREATE TABLE products (
     id int(20) NOT NULL AUTO_INCREMENT,
     asin      VARCHAR(255),
     sku       VARCHAR(255) UNIQUE,
     weight    int(10),
     shipping_method int(10),
     created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     updated_at  TIMESTAMP ,
     PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
