<?php
require_once dirname(__FILE__).'/vendor/autoload.php';
require_once(dirname(__FILE__).'/spyc.php');
require_once(dirname(__FILE__).'/lib/util.php');
$pdo = create_db();

$loader = new Twig_Loader_Filesystem(__DIR__ . '/tmpl');
$twig = new Twig_Environment($loader);
$template = $twig->loadTemplate("products.html.twig");
$products_weight = get_products_weight($pdo);
$template->display(
                   array(
                         'products' => get_products($pdo),
                         'weight'   => $products_weight
                         )
);
