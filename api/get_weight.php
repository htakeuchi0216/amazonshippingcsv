<?php
require_once('../lib/util.php');
$sku = $_POST['sku'];
$shipping_method = $_POST['shipping_method'];
$skus = preg_split("/,/", $sku);
$skus = array_filter($skus);# 空白要素を削除
if( empty($skus) || count($skus)!=1 ){
  //print "error";
  exit();
}
$pdo = create_db();
$stmt = $pdo->prepare("SELECT weight FROM products_weight WHERE sku = ? and shipping_method = ? ");
$stmt->execute(array($skus[0],$shipping_method ));
if($result = $stmt->fetch(PDO::FETCH_ASSOC)){
  print $result['weight'];
}