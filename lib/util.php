<?php
require_once(dirname(__FILE__).'/../spyc.php');
$config = array(
                "1" => "EMS",
                "2" => "SAL",
                "3" => "eパケ",
                "4" => "国際小包"
);

$debug = 0;

function get_rm_target_file($dir){
  $rm_target = "";
  $files = array();

  $res_dir = opendir( $dir );
  while( $file_name = readdir( $res_dir ) ){
    if($file_name == "." || $file_name == ".."){
      continue;
    }
    $files[]= $file_name;
    if(!$rm_target && $file_name){
      $rm_target = $file_name;
    }
  }
  return array($rm_target,$files);
}

function pp($val){
  print "<pre>";
  print_r($val);
  print "</pre>";
}

function get_products_weight($pdo){
  $data = array();
  $result = $pdo->query('SELECT * FROM products_weight ');
  foreach($result as $r){
    if( empty($data[ $r['sku']]) ){
      $data[ $r['sku'] ] = array();
    }
    $data[ $r['sku'] ][ $r['shipping_method'] ] = $r['weight'];
  }
  #print "<pre>";print_r($data);print "</pre>";
  return $data;
}
function get_products($pdo){
  $data = array();
  return $pdo->query('SELECT * FROM products WHERE shousai_type is not NULL');
  /*
  foreach ( as $row) {
    $data[ $row['sku'] ] = array(
                                 'sku'    => $row['sku'],
                                 'name'   => $row['name'],
                                 'weight' => $row['weight']
                                 );
  }
  return $data;

  return array("111","2222");
  */
}

function get_available_shipping($country){
}

function calc_shipping_price($country,$method,$weight){
  $conf     = Spyc::YAMLLoad(dirname(__FILE__)."/../conf/conf.yaml");
  $shipping = Spyc::YAMLLoad(dirname(__FILE__)."/../conf/shipping.yml");
  $price    = $shipping['price'];
  if( !$shipping['method'][$method]) {
    # ERROR
    return "";
  }
  if( $type = $conf[$country][$method] ){
    if($debug == 1){
      print "country : $country <br/>";
      print "method  : $method  <br/>";
      print "area    : $type  <br/>";
      print "weight  : $weight <BR/>";
    }

    foreach ( $price[$method] as $key => $val ){
      $range =preg_split("/-/", $key);
      $min = $range[0];
      $max = $range[1];
      #print "$min <  xx < $max <Br/>";
      if( $min <= $weight && $weight <= $max){
        #print_r($val);
        return $val[$type];
      }
    }
  }
  return "--";
}

function create_db(){
  try {
    $pdo = new PDO('mysql:host=localhost;dbname=amazon_shipping_tool;charset=utf8',
                   'amazon_user','amazon_user_pass',
                   array(PDO::ATTR_EMULATE_PREPARES => false));
  } catch (PDOException $e) {
    exit('データベース接続失敗。'.$e->getMessage());
  }
  return $pdo;
}

function get_product_sku_hash($pdo){
  $data = array();
  foreach ($pdo->query('SELECT sku,name,weight,shousai_type FROM products ') as $row) {
    $data[ $row['sku'] ] = array(
                                 'sku'    => $row['sku'],
                                 'name'   => $row['name'],
                                 'weight' => $row['weight'],
                                 'shousai_type' => $row['shousai_type']
                                 );
  }
  return $data;
}

function get_products_by_order_id_num($pdo,$order_id_num){
  $info =  array(
                 'qty' => 0,
                 'item_cnt' => 0,
                 'sum' => 0,
                 'avg' => 0,
                 'items' => array(),
                 );
  $stmt = $pdo->query("SELECT item_price,item_tax,product_name,sku,quantity_purchased,currency,shipping_price
   FROM orders WHERE order_id_num=$order_id_num ORDER BY id ");
  foreach( $stmt as $product ) {
    $info['item_cnt'] += 1;
    $info['sum']      += $product['item_price'];
    $info['qty']      += $product['quantity_purchased'];
    $info['items'][]  = $product;

  }
  /*
print "@@@@@@@@@@@@@@@@@";
    print "<pre>";
    print_r($info);
    print "</pre>";
  */
    return $info;
}

function find_order_by_order_id_num($pdo,$order_id_num){
  # XXX TODO place holderに変更
  $stmt = $pdo->query("SELECT * FROM orders WHERE order_id_num=$order_id_num LIMIT 1");
  $order = $stmt->fetch(PDO::FETCH_ASSOC);
  return $order;
}

function find_order_by_id($pdo,$id){
  # XXX TODO place holderに変更
  $stmt = $pdo->query("SELECT * FROM orders WHERE id=$id");
  $order = $stmt->fetch(PDO::FETCH_ASSOC);
  return $order;
}

function get_orders2($pdo){
  #return array(1,2);
  $tmp_orders = get_orders($pdo);
  $orders = array();
  foreach( $tmp_orders as $order){
    $orders[ $order['order_id'] ] = array(
                                          'qty' => 0,
                                          'item_cnt' => 0,
                                          'sum' => 0,
                                          'avg' => 0,
                                          'items' => array(),
                                          );
  }
  foreach( $tmp_orders as $order){
    #print "<pre>";print_r($order);print "</pre>";

    $orders[ $order['order_id'] ]['item_cnt'] += 1;
    $orders[ $order['order_id'] ]['sum'] += $order['item_price'];
    $orders[ $order['order_id'] ]['qty'] += $order['quantity_purchased'];
    $orders[ $order['order_id'] ]['items'][] = $order;
    /*    $orders[ $order['order_id'] ]['items'][] = 
      array(
            'buyer_name'   => $order['buyer_name'],
            'sku'          => $order['sku'],
            'product_name' => $order['product_name'],
            'quantity_purchased' => $order['uantity_purchased'],
            'item_price'         => $order['item_price'],
            'ship_service_level' => $order['ship_service_level'],
            );
    */

    #$#orders[ $order['order_id'] ]['data'][] = $order;
    #p#rint $order['order_id']."<BR>";
    #$orders[$order['6024419-9552233']
    #print "<pre>";print_r($order);print "</pre>";
    }
  #print "<pre>";print_r($orders);print "</pre>";



  return $orders;
}
function get_orders($pdo){
  $data = array();
  $sql = 'SELECT * FROM orders ORDER BY seq ';
  foreach ($pdo->query($sql) as $row) {
    $data[] = $row;
  }
  #$param['data'] = array(
  #                       array( "id" => 1 , "order_id" => "1111111",),
  #                       array( "id" => 2 , "order_id" => "2222", ),
  #                       array( "id" => 3 , "order_id" => "33333", ),
  #                       array( "id" => 4 , "order_id" => "4444", ),
  #);
  return $data;
}

function get_dollar_rate($pdo){
  $stmt = $pdo->query("SELECT * FROM dollar_rate WHERE id=1");
  $dollar_rate = $stmt->fetch(PDO::FETCH_ASSOC);
  return $dollar_rate;
}