<?php
require_once('./lib/util.php');
$pdo = create_db();
$debug = 0;
$key_area = array(
                  'order-id'            => null, 
                  'order-item-id'       => null, 
                  'purchase-date'       => null, 
                  'payments-date'       => null, 
                  'buyer-email'         => null, 
                  'buyer-name'          => null, 
                  'buyer-phone-number'  => null, 
                  'sku'                 => null, 
                  'product-name'        => null, 
                  'quantity-purchased'  => null, 
                  'currency'            => null, 
                  'item-price'          => null, 
                  'item-tax'            => null, 
                  'shipping-price'      => null, 
                  'shipping-tax'        => null, 
                  'gift-wrap-price'     => null, 
                  'gift-wrap-tax'       => null, 
                  'ship-service-level'  => null, 
                  'recipient-name'      => null, 
                  'ship-address-1'      => null, 
                  'ship-address-2'      => null, 
                  'ship-address-3'      => null, 
                  'ship-city'           => null, 
                  'ship-state'          => null, 
                  'ship-postal-code'    => null, 
                  'ship-country'        => null, 
                  'ship-phone-number'   => null, 
                  'gift-wrap-type'      => null, 
                  'gift-message-text'   => null, 
                  'item-promotion-discount' => null, 
                  'item-promotion-id'       => null, 
                  'ship-promotion-discount' => null, 
                  'ship-promotion-id'       => null, 
                  'delivery-start-date'     => null, 
                  'delivery-end-date'       => null, 
                  'delivery-time-zone'      => null, 
                  'delivery-Instructions'   => null, 
                  'sales-channel'         => null, 
                  'earliest-ship-date'      => null, 
                  'latest-ship-date'        => null, 
                  'earliest-delivery-date'  => null, 
                  'latest-delivery-date' => null,
);
#pp($key_area);

function p($str){
  global $debug;
  if($debug){
    print $str;
  }
}
function h($str) {
  return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}

function save_or_update_products($row,$pdo){
  $sku_hash = get_product_sku_hash($pdo);
  $sku          = $row[7];
  $product_name = $row[8];
  # XXX sku validation check
  #print "<pre>";print_r($row);print "</pre>";
  p("SKU : $sku <BR/>");

   if($sku_hash[$sku] ){ 
     p("$sku is already exists. not insert");
   } 
   else{ 
     p("INSERT INTO products(sku,name) VAKUES( $sku , $product_name )");
    $stmt = $pdo->prepare("INSERT INTO products(sku,name) VALUES(?,?);");
    $flag = $stmt->execute( array($sku, $product_name) );
    if ($flag){
      p('productsの追加に成功しました<br>');
    }else{
      p('productsの追加に失敗しました<br>');
    }
  }
}

function save_order($row,$pdo,$seq){
  global $key_area;

  #print "<BR/>KKKKKKKKKKKKEY AREA<BR/>";
  #pp($key_area);
  $indexes = array(
                $key_area['order-id'], # 0
                $key_area['order-item-id'], #1
                $key_area['purchase-date'], #2
                $key_area['payments-date'], #3
                $key_area['buyer-email'],   #4
                $key_area['buyer-name'],    #5
                $key_area['buyer-phone-number'],#6
                $key_area['sku'], #7
                $key_area['product-name'], #8
                $key_area['quantity-purchased'], #9
                $key_area['currency'], #10
                $key_area['item-price'], #11
                $key_area['item-tax'], #12
                $key_area['shipping-price'], #13
                $key_area['shipping-tax'],
                $key_area['gift-wrap-price'],#15
                $key_area['gift-wrap-tax'],
                $key_area['ship-service-level'],
                $key_area['recipient-name'],
                $key_area['ship-address-1'],
                $key_area['ship-address-2'],#20
                $key_area['ship-address-3'],
                $key_area['ship-city'],
                $key_area['ship-state'],
                $key_area['ship-postal-code'],
                $key_area['ship-country'],#25
                $key_area['ship-phone-number'],
                $key_area['gift-wrap-type'],
                $key_area['gift-message-text'],
                $key_area['item-promotion-discount'],
                $key_area['item-promotion-id'],#30
                $key_area['ship-promotion-discount'],
                $key_area['ship-promotion-id'],
                $key_area['delivery-start-date'],
                $key_area['delivery-end-date'],
                $key_area['delivery-time-zone'],#35
                $key_area['delivery-Instructions'],
                $key_area['sales-channel'],
                $key_area['earliest-ship-date'],
                $key_area['latest-ship-date'],
                $key_area['earliest-delivery-date'],#40
                $key_area['latest-delivery-date'],# 41
                );
  $data = array();
  foreach ($indexes as $index){
    $data[] = $row[$index];
    #print "-- $index <BR/>";
  }
  $data[] = $seq;
  $data[] = str_replace("-", "", $data[0] ); #order_id_num
  if($data[10] != "USD"){# currency
    print "[ERR10]";
    exit();
  }
  if(!$data[42] || !is_numeric($data[42] ) ){# seq
    print "[ERR42]";
    exit();
  }
  if(!$data[43] || !is_numeric($data[43] ) ){ #order_id_num
    print "[ERR43]";
    exit();
  }

  p("order_id:  $data[0] , order_item_id $data[1]  <BR/>");

  $sql = 'insert into orders (
  order_id,order_item_id,purchase_date,payments_date,buyer_email,
  buyer_name,buyer_phone_number,sku,product_name,quantity_purchased,
  currency,item_price,item_tax,shipping_price,shipping_tax,
  gift_wrap_price,gift_wrap_tax,ship_service_level,recipient_name,ship_address_1,
  ship_address_2,ship_address_3,ship_city,ship_state,ship_postal_code,
  ship_country,ship_phone_number,gift_wrap_type,gift_message_text,item_promotion_discount,
  item_promotion_id,ship_promotion_discount,ship_promotion_id,delivery_start_date,delivery_end_date,
  delivery_time_zone,delivery_Instructions,sales_channel,earliest_ship_date,latest_ship_date,
  earliest_delivery_date,latest_delivery_date,seq,order_id_num
)
values (
?,?,?,?,?,?,?,?,?,?,
?,?,?,?,?,?,?,?,?,?,
?,?,?,?,?,?,?,?,?,?,
?,?,?,?,?,?,?,?,?,?,
?,?,?,?
)';
  $stmt = $pdo->prepare($sql);
#  $flag = $stmt->execute(array( 'test@test.com'));
#  $row[] = $seq;
#  $row[] = str_replace("-", "", $row[0] ); #order_id_num
  #pp($data);
  $flag = $stmt->execute($data);
  if ($flag){
    p('データの追加に成功しました<br>');
  }else{
    p('データの追加に失敗しました<br>');
  }

}

// アップロードファイル情報を表示する。
if($debug){
  echo "アップロードファイル名　：　" , $_FILES["file_data1"]["name"] , "<BR>";
  echo "MIMEタイプ　：　" , $_FILES["file_data1"]["type"] , "<BR>";
  echo "ファイルサイズ　：　" , $_FILES["file_data1"]["size"] , "<BR>";
  echo "テンポラリファイル名　：　" , $_FILES["file_data1"]["tmp_name"] , "<BR>";
  echo "エラーコード　：　" , $_FILES["file_data1"]["error"] , "<BR>";
}
// アップロードファイルを格納するファイルパスを指定
$filename = "/tmp/" . $_FILES["file_data1"]["name"];

if ( $_FILES["file_data1"]["size"] === 0 ) {
  echo "ファイルはアップロードされてません！！ アップロードファイルを指定してください。";
} else {
  // アップロードファイルされたテンポラリファイルをファイル格納パスにコピーする
  $result = @move_uploaded_file( $_FILES["file_data1"]["tmp_name"], $filename);
  if ( $result === true ) {
    p( "アップロード成功！！");

    $fp = fopen($filename, 'rb');
    # Transaction Start
    $pdo->beginTransaction();
    try{
      $sql = 'DELETE FROM orders';
      $stmt = $pdo->prepare($sql);
      $flag = $stmt->execute();
      if ($flag){
        p('ordersの削除に成功しました<br>');
      }else{
        print('ordersの削除に失敗しました<br>');
        exit();
      }
      $cnt = 0;
      while ($row = fgetcsv($fp,0,"\t")){
        $cnt++;
        if($cnt == 1 ){
          for ($i=0;$i<count($row);$i++){
            $key = $row[$i];
            $key_area[$key] = $i;
            p( $i."==". $row[$i] ."\n");
          }
          p( "`@@@@@@@@@@@@@@@@@\n");
          p( print_r( $key_area,true) );
          continue;
        }
        $seq = $cnt - 1;
        save_order($row,$pdo,$seq);
        save_or_update_products($row,$pdo);
      }

      #COMMIT
      # Transaction END
      p("COMMIT");
      $pdo->commit();
      header("Location: /?sc=1"); 
      exit();
    }
    catch (Exception $e) {
      $pdo->rollback();
      throw $e;
      print "ERROR";
      exit();
    }
  } else {
    echo "アップロード失敗！！";
  }
}

#print_r($_FILES);
